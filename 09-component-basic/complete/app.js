'use strict';

var baconIpsumComponent = {
  bindings: {
    title: '<',
    titleTwoWay: '=',
    paragraphs: '@',
    onChange: '&'
  },
  templateUrl: 'baconIpsum.html',
  controller: function(generator) {
    this.$onChanges = function(changesObj) {
      if (changesObj.paragraphs) {
        this.data = generator.getParagraphs(this.paragraphs);
        this.onChange({data: this.data});
      }
    };
  },
  controllerAs: 'bacon'
};

function BaconController() {
  this.title = 'bacon-ipsum s title';
  this.paragraphs = 3;

  this.onChangeCallback = function(data) {
    console.log('Loaded paragraphs', data);
  }
}

angular.module('bacon', ['ipsumService'])
  .controller('BaconController', BaconController)
  .component('baconIpsum', baconIpsumComponent);


