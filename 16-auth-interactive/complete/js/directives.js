'use strict';

function authInfoDirective(authService) {
  return {
    scope: true,
    link: function(scope) {
      scope.auth = authService
    },
    template: '{{auth.getUserName()}} <a class="btn-logout" ng-click="auth.logout()">Logout</a>'
  };
}

function authIsAuthenticatedDirective(authService) {
  return {
    scope: true,
    link: function(scope, element, attrs) {
      var authenticatedState = attrs.authIsAuthenticated === 'false';

      function changeVisibility() {
        var toggleHide = authService.isAuthenticated() === authenticatedState;

        element.toggleClass("auth-hide", toggleHide);
      }

      changeVisibility();

      scope.$on("login:loginSuccess", changeVisibility);
      scope.$on("login:loggedOut", changeVisibility);
    }
  };
}

angular.module('authApp')
  .directive("authInfo", authInfoDirective)
  .directive("authIsAuthenticated", authIsAuthenticatedDirective);
