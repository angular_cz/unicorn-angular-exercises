'use strict';

function configRouter($routeProvider, $locationProvider) {

  $locationProvider.hashPrefix('');

  // TODO 1. definujte routu pro OrderListController
  // TODO 2. zajistěte přesměrování na seznam
  // TODO 3. definujte routu pro OrderDetailController
  // TODO 4.1. vytvořte routu pro vytvoření záznamu
}

function OrderListController($http, REST_URI) {
  this.orders = [];

  this.statuses = {
    NEW: 'Nová',
    MADE: 'Vyrobená',
    CANCELLED: 'Zrušená',
    PAID: 'Zaplacená',
    SENT: 'Odeslaná'
  };

  this.onOrdersLoad = function(orders) {
    this.orders = orders;
  };

  $http.get(REST_URI + '/orders')
    .then(function(response) {
      return response.data;
    })
    .then(this.onOrdersLoad.bind(this));

}

function OrderDetailController(orderData) {
  this.order = orderData;
}

function OrderCreateController($http, $location, REST_URI) {
  this.save = function(order) {

    // TODO 4.3. proveďte uložení záznamu
    // TODO 4.4. po uložení přesměrujte na seznam

  }
}
angular.module('orderAdministration', ['ngRoute', 'ngMessages'])
  .constant('REST_URI', '//angular-cz-orders-api.herokuapp.com')
  .config(configRouter)
  .controller('OrderCreateController', OrderCreateController)
  .controller('OrderListController', OrderListController)
  .controller('OrderDetailController', OrderDetailController);
